var elixir = require('laravel-elixir');

elixir(function(mix){
    mix.styles([
        'animate.css',
        'custom.css',
        'style.default.css',
        'style.violet.css',
        'owl.carousel.css',
        'owl.theme.css',
    ]);

    mix.scripts([
		'jquery.cookie.js',
		'waypoints.min.js',
		'jquery.counterup.min.js',
		'jquery.parallax-1.1.3.js',
        'jquery.mask.min.js',
		'owl.carousel.min.js',
		'front.js',
        'custom.js'
    ]);
});