<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $items = \App\Models\Product::getActiveItems();

    return view('index')->with('products', $items);
});

/**
 * Product routes
 */
Route::get('/product', 'ProductsController@showcase');
Route::get('/product/{slug}', 'ProductsController@display');

/**
 * Seller Routes
 */
Route::get('/seller/{slug}', 'SellerController@listProducts');

/**
 * Cart Routes
 */
Route::post('/store', 'CartController@store');
Route::get('/forget', 'CartController@forget');

/**
 * Checkout Routes
 */
Route::get('/checkout', 'CheckoutController@checkout');
Route::post('/pay', 'CheckoutController@doPay');

/**
 * Tests
 */
Route::get('/test', function (){
    $paymentRecipient = new \App\Entities\Payment\Recipient\PaymentRecipient(new \PagarMe\Sdk\PagarMe(env('API_KEY')));
    $a = $paymentRecipient->get();

    dd($a);
});
