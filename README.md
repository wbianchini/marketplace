# Marketplace

Marketplace para venda e aluguel de fantasias

## Desafio proposto

Maria Barros decidiu abrir sua startup de aluguel de fantasias de carnaval, onde outros fornecedores também poderão vender suas fantasias conforme combinado. As regras de venda são:

```
- Para cada fantasia alugada produzida pela Maria, o valor total da venda + R$ 42,00 fixo de frete pertencem à Maria.
- Para cada fantasia alugada de outros fornecedores, o valor total da venda deverá ser dividido da seguinte forma:
- 15% do valor do produto vai para a Maria.
85% + R$ 42,00 de frete para o parceiro.
```

### Instalação

É necessário para a instalação:
 
- Fazer uma cópia do arquivo .env.example com o nome .env

```
Nesse arquivo é importante substituir as informações do banco de dados.
Durante o desenvolvimento foi utilizado o PostgreSQL, mas é possível rodar com o de sua preferência.
```

```
Também é importante substituir a variavel de ambiente API_KEY que segura a informação da chave de api da pagarme.
```

- Executar os seguintes comandos:
```
$ php artisan migrate; // Para criar a estrutura do banco
$ php artisan db:seed; // Popular o banco com algumas informações importantes

# Foram criadas algumas factories para popular as tabelas e ser possível rodar a aplicação
#disponiveis em database/factories/
```

Após isso, resta apenas servir a aplicação executando, caso necessario rodar um teste funcional.
```
$ php artisan serve; // Na pasta raíz do projeto
```
ou

```
$ php -S localhost:3030; // Na pasta public
# Pode ser utilizada qualquer porta
```


### Informações

Foram desenvolvidas página inicial com listagem de todos os produtos e página de checkout.

É possível comprar mais de um produto e produtos fornecidos por diferentes fabricantes.

Assim que clicado no botão de adicionar ao carrinho, o item é adicionado na sessão e o usuário é redirecionado para a pagina de checkout.
A qualquer momento é possível retornar à pagina de listagem de produtos e selecionar um novo item.

A divisão do valor entre os fornecedores acontece na finalização do pedido e é feita por valor. 
Tudo isso definido de acordo com split rules descritas na documentação pagarme.

## Running the tests

Para rodar os testes basta executar o phpunit na pasta raiz do projeto.
Recomento usar o phpunit que instalado junto ao laravel, executando "$ ./vendor/bin/pphpunit"


## Built With

* [Laravel 5.5](https://laravel.com/docs/5.5/releases) - The PHP framework used
* [Pagar.Me sdk ^3.5](https://github.com/pagarme/pagarme-php) - SDK pagarme last version
* [Bootstrapious template](https://bootstrapious.com/) - Html Template used
