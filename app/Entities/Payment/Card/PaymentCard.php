<?php

namespace App\Entities\Payment\Card;

use PagarMe\Sdk\Card\Card;
use PagarMe\Sdk\PagarMe;

class PaymentCard
{
    /** @var \PagarMe\Sdk\PagarMe */
    private $pagarMe;
    private $information;

    public function __construct(PagarMe $pagarMe, $cardInformation = null)
    {
        $this->pagarMe = $pagarMe;
        $this->information = $cardInformation;
    }

    public function get() : Card
    {
        return $this->pagarMe->card()->create(
            preg_replace("/\s+/", "", $this->information['cardNumber']),
            $this->information['holderName'],
            $this->information['month'] . substr($this->information['year'], -2)
        );
    }
}