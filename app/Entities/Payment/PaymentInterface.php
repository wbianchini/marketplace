<?php
/**
 * Created by PhpStorm.
 * User: wBian
 * Date: 11/15/2017
 * Time: 3:16 PM
 */

namespace App\Entities\Payment;


use App\Entities\Cart\Cart;

interface PaymentInterface
{
    public function process(Cart $cart);

    public function getStatus();
}