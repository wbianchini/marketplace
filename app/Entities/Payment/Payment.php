<?php

namespace App\Entities\Payment;

use App\Entities\Cart\Cart;
use App\Entities\Payment\Card\PaymentCard;
use App\Entities\Payment\Customer\PaymentCustomer;
use App\Entities\Payment\SplitRules\SplitRules;
use App\Models\PaymentConfiguration;
use PagarMe\Sdk\{
    PagarMe, Customer\Customer, SplitRule\SplitRule
};


class Payment implements PaymentInterface
{
    /** @var PagarMe */
    private $pagarMe;

    /** @var \App\Entities\Cart\Cart */
    private $cart;

    private $customer = [];

    private $address = [];

    private $creditCard = [];

    private $status;

    /** @var  array */
    private $metadata;

    public function __construct()
    {
        $this->pagarMe = new PagarMe( $this->getCredentials()->api_key );
    }

    public function process(Cart $cart)
    {
        $this->cart = $cart;
        $this->constructMetadata();

        $this->creditCardPayment();
    }

    /**
     * Split by value
     * @return array|null
     */
    private function defineSplitRules() : ?array
    {
        $splitRules = new SplitRules($this->pagarMe, $this->cart);

        return $splitRules->define();
    }

    private function creditCardPayment()
    {
        $card = (new PaymentCard($this->pagarMe, $this->creditCard))->get();

        $transaction = $this->pagarMe->transaction()->creditCardTransaction(
            $this->cart->getTotalAmountInCents(),
            $card,
            $this->getCustomer(),
            $this->creditCard['cvv'],
            true,
            Payment::POSTBACK_URL,
            $this->metadata,
            [
                'splitRules' => $this->defineSplitRules()
            ]
        );

        $this->setStatus($transaction->getStatus());
    }

    private function boletoPayment()
    {
        #TODO implement
        return;

        $transaction = $this->pagarMe->transaction()->boletoTransaction(
            $this->cart->getTotalAmountInCents(),
            $this->getCustomer(),
            Payment::POSTBACK_URL,
            $this->metadata
        );

        dd($transaction);
    }

    /**
     * @return Customer
     */
    private function getCustomer() : Customer
    {
        $customerInfo = [
            'customer' => $this->customer,
            'address'  => $this->address
        ];
        $paymentCustomer = new PaymentCustomer($this->pagarMe, $customerInfo);

        return $paymentCustomer->get();
    }

    private function getCredentials() : \stdClass
    {
        $keys = PaymentConfiguration::first();

        if (empty($keys))
        {
            $keys = new \stdClass();
            $keys->api_key = env('API_KEY');
        }

        return $keys;
    }

    private function constructMetadata() : void
    {
        $this->metadata = [];

        foreach ($this->cart->getItems() as $product)
        {
            $this->metadata[] = ['product_id' => $product->id];
        }
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param mixed $creditCard
     */
    public function setCreditCard($creditCard)
    {
        $this->creditCard = $creditCard;
    }

    private function setStatus($transaction_status)
    {
        $this->status = $transaction_status;
    }

    public function getStatus()
    {
        // TODO: Implement getStatus() method.
    }

    const POSTBACK_URL   = '127.0.0.1:8000/process_request';
    const SHIPMENT_PRICE = 42.00;
}