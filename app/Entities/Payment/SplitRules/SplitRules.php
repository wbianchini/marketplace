<?php

namespace App\Entities\Payment\SplitRules;

use App\Entities\Cart\Cart;
use App\Entities\Payment\Payment;
use App\Entities\Payment\Recipient\PaymentRecipient;
use App\Models\Product;
use App\Models\Seller;
use App\Models\SellerCategory;
use PagarMe\Sdk\PagarMe;
use PagarMe\Sdk\Recipient\Recipient;

class SplitRules
{
    /** @var \PagarMe\Sdk\PagarMe */
    private $cart;

    /** @var Cart */
    private $pagarMe;

    private $ownerAmount;

    private $sellers;

    private $split_rules;

    public function __construct(PagarMe $pagarMe, Cart $cart)
    {
        $this->pagarMe = $pagarMe;
        $this->cart = $cart;
        $this->ownerAmount = 0;

        $this->sellers = [];
        $this->setOwner();
    }

    public function define()
    {
        $this->calculateSplitByProduct();

        $this->createSplitRules();

        return $this->split_rules;
    }

    private function createSplitRules()
    {
        foreach ($this->sellers as $seller_id => $seller) {

            #TODO define shipment rules based on different recipients.

            $this->split_rules[] = $this->pagarMe->splitRule()->monetaryRule(
                $seller['value'] + Payment::SHIPMENT_PRICE,
                $this->getRecipient(),
                true,
                true
            );
        }
    }

    /**
     * Calculate the split amount by product
     *
     */
    private function calculateSplitByProduct()
    {
        foreach ($this->cart->getItems() as $item) {

            if(empty($this->sellers[$item->seller->id]))
            {
                $this->sellers[$item->seller->id] = [ 'value' => 0 ];
            }

            $this->sellers[$item->seller->id]['value'] += $this->calculateItemSplitValue($item, $item->seller->category);
        }
    }

    /**
     * Divide product total amount between seller and owner, based seller category
     *
     * @param Product $item
     * @param SellerCategory $category
     * @return mixed
     */
    private function calculateItemSplitValue(Product $item, SellerCategory $category)
    {
        $seller_split_amount = $item->price * $category->percentage;
        $this->ownerAmount   = $item->price - $seller_split_amount;

        return $seller_split_amount;
    }

    /**
     * Search for marketplace owner and create a index
     *
     */
    private function setOwner()
    {
        $owner = Seller::getOwner();

        $this->sellers[$owner->id] = ['value' => 0 ];
    }

    private function getRecipient()
    {
        $paymentRecipient = new PaymentRecipient($this->pagarMe, []);

        return $paymentRecipient->get();
    }
}