<?php

namespace App\Entities\Payment\Recipient;

use App\Models\Seller;
use PagarMe\Sdk\BankAccount\BankAccount;
use PagarMe\Sdk\PagarMe;
use PagarMe\Sdk\Recipient\Recipient;

class PaymentRecipient
{
    /** @var \PagarMe\Sdk\PagarMe */
    private $pagarMe;
    private $information;

    public function __construct(PagarMe $pagarMe, Seller $recipientInformation = null)
    {
        # todo attach recipient to an Seller

        $this->pagarMe = $pagarMe;
        $this->information = $recipientInformation;
    }

    public function get() : Recipient
    {
        return $this->findExistentRecipient() ?? $this->createNewRecipient();
    }

    private function findExistentRecipient() : ?Recipient
    {
        return null;
    }

    /**
     * @return null|Recipient
     */
    private function createNewRecipient() : ?Recipient
    {
        return $this->pagarMe->recipient()->create(
            $this->getBankAccount(),
            'monthly',
            13,
            true,
            true,
            42
        );
    }

    /**
     * @return null|BankAccount
     */
    public function getBankAccount() : ?BankAccount
    {
        # todo get bank account from the seller information

        return $this->pagarMe->bankAccount()->create(
            '341',
            '0932',
            '58054',
            '5',
            '26268738888',
            'API BANK ACCOUNT',
            '1'
        );
    }
}