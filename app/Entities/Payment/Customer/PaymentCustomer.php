<?php

namespace App\Entities\Payment\Customer;

use PagarMe\Sdk\{PagarMe};
use PagarMe\Sdk\Customer\{
    Address, Customer, Phone
};

class PaymentCustomer
{
    /** @var \PagarMe\Sdk\PagarMe */
    private $pagarMe;

    private $information;

    public function __construct(PagarMe $pagarMe, $customerInformation = null)
    {
        $this->pagarMe = $pagarMe;
        $this->information = $customerInformation;
    }

    public function get() : Customer
    {
        return $this->findExistentCustomer() ?? $this->createNewCustomer();
    }

    public function findExistentCustomer() : ?Customer
    {
        return null;
    }

    public function createNewCustomer() : Customer
    {
        return $this->pagarMe->customer()->create(
            $this->information['customer']['name'],
            $this->information['customer']['email'],
            $this->information['customer']['document'],
            $this->buildCustomerAddress(),
            $this->buildCustomerPhone(),
            str_replace("/","",$this->information['customer']['birthday']),
            $this->information['customer']['gender']
        );
    }

    private function buildCustomerAddress() : ?Address
    {
        $address = new Address([
            'street'        => $this->information['address']['street'],
            'streetNumber'  => $this->information['address']['streetNumber'],
            'neighborhood'  => $this->information['address']['neighborhood'],
            'zipcode'       => $this->information['address']['zipcode'],
            'complementary' => '',
            'city'          => $this->information['address']['city'],
            'state'         => $this->information['address']['state'],
            'country'       => self::DEFAULT_COUNTRY,
        ]);

        return $address;
    }

    private function buildCustomerPhone() : ?Phone
    {
        $phone = new Phone([
            'ddd'    => '11',
            'number' => '975493188'
        ]);

        return $phone;
    }

    const DEFAULT_COUNTRY = 'Brazil';
}