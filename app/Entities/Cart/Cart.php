<?php

namespace App\Entities\Cart;

use App\Models\Product;

class Cart
{
    /**
     * @var ProductCollection $items
     */
    private $items;

    /** @var  integer $totalQuantity */
    private $totalQuantity;

    /** @var  float $totalAmount */
    private $totalAmount;

    /**
     * Cart constructor
     *
     * @param Cart|null $cart Containing possible current session cart
     */
    public function __construct(Cart $cart = null)
    {
        $this->items = new ProductCollection;

        if(!empty($cart))
        {
            $this->items =  $cart->getItems();
        }
    }

    /**
     * Add a new item to Cart's products collection
     *
     * @param Product $product
     * @return bool
     */
    public function add(Product $product) : bool
    {
        $this->items->push($product);

        $this->calculateTotals();
        return true;
    }

    public function remove(Product $product)
    {
        $item = $this->getItems()->findBy($product->id);

        if(empty($item['key'])) return false;

        $this->getItems()->pull($item['key']);
    }

    public function calculateTotals()
    {
        $totalQuantity  = 0;
        $totalAmount    = 0;
        foreach ($this->getItems() as $product)
        {
            $totalQuantity  += (int) $product->quantity;
            $totalAmount    += (float) $product->price;
        }

        $this->totalAmount   = $totalAmount;
        $this->totalQuantity = $totalQuantity;
    }

    public function getPublicSummary()
    {
        $this->calculateTotals();

        $public_summary = new \stdClass();
        $public_summary->totalAmount = $this->totalAmount;
        $public_summary->totalQuantity = $this->totalQuantity;

        return $public_summary;
    }

    /**
     * Returns a collection containing all cart stored items
     *
     * @return ProductCollection
     */
    public function getItems() : ProductCollection
    {
        return $this->items;
    }

    /**
     * Get total quantity of stored items
     *
     * @return int
     */
    public function getTotalQuantity() : int
    {
        return $this->totalQuantity;
    }

    /**
     * Get total quantity of stored items
     *
     * @return float
     */
    public function getTotalAmount() : float
    {
        return $this->totalAmount;
    }

    /**
     * Get Total Amount converted in cents
     *
     * @return int
     */
    public function getTotalAmountInCents() : int
    {
        return (int) ($this->totalAmount * 100);
    }

    /**
     * Checks if session already has a Cart key
     *
     * @return bool
     */
    public static function exists() : bool
    {
        return session()->has('cart');
    }

    public function __destruct()
    {
        session()->put('cart', $this);
    }
}