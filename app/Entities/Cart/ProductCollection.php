<?php

namespace App\Entities\Cart;

use App\Exceptions\InvalidCartItemException;
use App\Models\Product;
use Illuminate\Support\Collection;

class ProductCollection extends Collection
{

    /**
     * The items contained in the collection.
     *
     * @var Product[]
     */
    protected $items = [];

    /**
     * ProductCollection constructor.
     * @param Product[] $items
     */
    public function __construct($items = [])
    {
        $new_items = $this->getArrayableItems($items);

        foreach ($new_items as $item)
        {
            self::push($item);
        }
    }

    /**
     * Push an item onto the end of the collection.
     *
     * @param  Product $item
     * @return $this
     */
    public function push($item)
    {
        if(!$item instanceof Product)
        {
            throw new InvalidCartItemException();
        }

        parent::push($item);
        return $this;
    }

    /**
     * Serch for a product with given id into items
     *
     * @param $id
     * @return Product|bool
     */
    public function findBy($id)
    {
        if(empty($this->items)) return false;

        foreach($this->items as $key => $product)
        {
            if($product->id != $id) continue;

            return ['key' => $key, 'product' => $product];
        }

        return false;
    }

    /**
     * Get an item at a given offset.
     *
     * @param  mixed  $key
     * @return Product
     */
    public function offsetGet($key) : Product
    {
        parent::offsetGet($key);
    }
}