<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    public function listProducts()
    {
        $products = Product::getBySlug($slug);

        return $products;
    }
}
