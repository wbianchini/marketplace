<?php

namespace App\Http\Controllers;

use App\Entities\Cart\Cart;
use App\Models\Product;
use Illuminate\Http\Request;


class CartController extends Controller
{
    public function store(Request $request)
    {
        $product_id = $request->product_id;
        $current_cart = null;
        if(Cart::exists())
        {
            $current_cart = session()->get('cart');
        }

        $product = Product::where('id', $product_id)->first();

        $cart = new Cart($current_cart);
        $cart->add($product);
        $cart->calculateTotals();

        session()->put('cart', $cart);
        session()->flush();
    }

    public function remove(Request $request, $product_id)
    {
        $product = Product::where('id', $product_id)->first();

        $cart = session()->get('cart');
        $cart->remove($product);

        return redirect('cart')->withSuccessMessage('Item has been removed!');
    }

    public function forget()
    {
        session()->forget('cart');
        session()->flush();
    }
}
