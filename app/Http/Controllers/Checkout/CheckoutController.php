<?php

namespace App\Http\Controllers;

use App\Entities\Cart\Cart;
use App\Entities\Payment\Payment;
use App\Models\Product;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function checkout()
    {
        // $this->validateCart();

        if(!Cart::exists()){
            return redirect('/')
                ->with('status', 'Empty Cart!');
        }

        /** @var Cart $cart */
        $cart = session()->get('cart');
        $cart->calculateTotals();

        $products = $cart->getItems();

        return view('checkout.checkout')->with('cart_items', $products)->with('cart_summary', $cart->getPublicSummary());
    }

    public function doPay(Request $request)
    {
        // $this->validateCart();

        if(!Cart::exists()){
            return redirect('/')
                ->with('status', 'Empty Cart!');
        }

        $cart = session()->get('cart');
        $payment = new Payment;

        $payment->setAddress($request->input('address'));
        $payment->setCustomer($request->input('customer'));
        $payment->setCreditCard($request->input('creditCard'));

        $payment->process($cart);

        // $payment->getStatus();
        #TODO handle status and redirect user with correct message

        $status = 'Pagamento efetuado com sucesso.';

        return redirect('/')->with('status', $status);
    }

    # todo
    private function validateCart()
    {
        if(Cart::exists())
        {
            return true;
        }

        return redirect('cart')
                ->with('status', 'Empty Cart!');
    }

    #todo persist order data
}
