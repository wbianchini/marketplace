<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function showcase()
    {
        $products = Product::getActiveItems();

        return $products;
    }

    public function display($slug)
    {
        $product = Product::getBySlug($slug);

        return $product;
    }

}
