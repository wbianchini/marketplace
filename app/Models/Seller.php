<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{

    /**
     * Search for the seller with Owner category
     *
     * @return Seller|null
     */
    public static function getOwner() : ?Seller
    {
        return Seller::join('seller_categories', 'sellers.category_id', '=', 'seller_categories.id')
            ->where('seller_categories.name', 'Owner')->first();
    }

    public function category()
    {
        return $this->belongsTo('\App\Models\SellerCategory');
    }
}
