<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Product extends Model
{
    /** @var int $quantity */
    public $quantity = 1;

    public static function getActiveItems() : Collection
    {
        return self::where('active', true)->get();
    }

    public static function getBySlug($slug) : Product
    {
        return self::where('slug', $slug)
                ->where('active', 1)
                ->first();
    }

    public static function getBySellerSlug($seller_slug) : Collection
    {
        #TODO implement
        self::join('sellers', 'products.seller_id', '=', 'sellers.id')
            ->where('sellers.slug', $seller_slug)
            ->get();
    }

    public function setQuantity(int $quantity) :void
    {
        $this->quantity = $quantity;
    }

    public function seller()
    {
        return $this->belongsTo('\App\Models\Seller');
    }
}