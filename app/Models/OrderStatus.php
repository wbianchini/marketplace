<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    public static function convertStatus($status) : int
    {
        $order_status = [
            'processing'      => OrderStatus::PROCESSING,
            'authorized'      => OrderStatus::AUTHORIZED,
            'paid'            => OrderStatus::PAID,
            'refunded'        => OrderStatus::REFUNDED,
            'waiting_payment' => OrderStatus::WAITING_PAYMENT,
            'pending_refund'  => OrderStatus::PENDING_REFUND,
            'refused'         => OrderStatus::REFUSED,
            'chargedback'     => OrderStatus::CHARGEDBACK,
        ];
        if(empty($order_status[$status])){
            return OrderStatus::PROCESSING;
        }

        return $order_status[$status];
    }

    const PROCESSING      = 1;
    const AUTHORIZED      = 2;
    const PAID            = 3;
    const REFUNDED        = 4;
    const WAITING_PAYMENT = 5;
    const PENDING_REFUND  = 6;
    const REFUSED         = 7;
    const CHARGEDBACK     = 8;
}