<?php

namespace Tests\Unit;

use App\Entities\Cart\ProductCollection;
use App\Exceptions\InvalidCartItemException;
use App\Models\Product;
use App\Models\Seller;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class ProductCollectionTest
 * @package Tests\Unit
 */
class ProductCollectionTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test if excepts if try to push a array
     *
     * @return void
     */
    public function testExceptsIfNotInstanceOfProducts() : void
    {
        $collection = new ProductCollection();

        $this->expectException(InvalidCartItemException::class);

        $seller = factory(Seller::class)->make();

        $collection->push($seller);
    }


    /**
     * Test if
     *
     * @return void
     */
    public function testAcceptsProductModel() : void
    {
        $product = factory(Product::class)->make();

        $collection = new ProductCollection();

        $collection->push($product);

        $this->assertInstanceOf(Product::class, $collection->last());
        $this->assertEquals($product, $collection->last());
    }

    public function testInstanceOfPushedItem() : void
    {
        $products = factory(Product::class, 2)->make();

        $collection = new ProductCollection($products);

        $this->assertInstanceOf(Product::class, $collection->last());
    }
}
