<?php

namespace Tests\Unit;

use App\Entities\Cart\Cart;
use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Assert integrity of last inserted item
     *
     * @return void
     */
    public function testLastInsertedItem()
    {
        $cart = new Cart;

        $product = factory(Product::class)->create();
        $cart->add($product);

        $this->assertInstanceOf(Product::class, $cart->getItems()->last());
        $this->assertEquals($product->id, $cart->getItems()->last()->id);
    }
}
