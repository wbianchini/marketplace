<?php

namespace Tests\Feature;

use App\Entities\Payment\Recipient\PaymentRecipient;
use PagarMe\Sdk\PagarMe;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentRecipientTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreatedRecipientHasId()
    {
        $pagarMe = new PagarMe( env("API_KEY") );

        $paymentRecipient = new PaymentRecipient($pagarMe);

        $createdRecipient = $paymentRecipient->get();

        $this->assertNotEmpty($createdRecipient->getId());
        $this->assertNotEmpty($createdRecipient->getBankAccount());
    }
}