<div class="row products">
    {{ csrf_field() }}
    @foreach($products as $product)
    <div class="col-md-3 col-sm-4">
        <div class="product">
            <div class="image">
                <a href="product/{{ $product->slug }}">
                    <img src="img/product2.jpg" alt="" class="img-responsive image1">
                </a>
            </div>
            <!-- /.image -->
            <div class="text">
                <h3><a href="product/{{ $product->slug }}">{{ $product->name }}</a></h3>
                <p>by <a href="seller/{{ $product->seller->slug }}">{{$product->seller->name }}</a></p>

                <p class="price"> R$ {{ number_format($product->price, 2, ",", ".") }}</p>

                <a class="btn btn-template-main add-to-cart" product_id="{{ $product->id }}">
                    <i class="fa fa-shopping-cart"></i>Comprar
                </a>
            </div>
            <!-- /.text -->

            {{--
                <div class="ribbon sale">
                    <div class="theribbon">SALE</div>
                    <div class="ribbon-background"></div>
                </div>
                <!-- /.ribbon -->

                <div class="ribbon new">
                    <div class="theribbon">NEW</div>
                    <div class="ribbon-background"></div>
                </div>
                <!-- /.ribbon -->
            --}}
        </div>
        <!-- /.product -->
    </div>
    @endforeach

    {{--
    <div class="col-md-3 col-sm-4">
        <div class="product">
            <div class="image">
                <a href="shop-detail.html">
                    <img src="img/product3.jpg" alt="" class="img-responsive image1">
                </a>
            </div>
            <!-- /.image -->
            <div class="text">
                <h3><a href="shop-detail.html">Black Blouse Versace</a></h3>
                <p class="price">$143.00</p>
                <a href="shop-basket.html" class="btn btn-template-main"><i class="fa fa-shopping-cart"></i>Comprar</a>
                <p class="buttons">
                    <a href="shop-detail.html" class="btn btn-default">View detail</a>
                </p>

            </div>
            <!-- /.text -->
        </div>
        <!-- /.product -->
    </div>

    <div class="col-md-3 col-sm-4">
        <div class="product">
            <div class="image">
                <a href="shop-detail.html">
                    <img src="img/product4.jpg" alt="" class="img-responsive image1">
                </a>
            </div>
            <!-- /.image -->
            <div class="text">
                <h3><a href="shop-detail.html">Black Blouse Versace</a></h3>
                <p class="price">$143.00</p>
                <a href="shop-basket.html" class="btn btn-template-main"><i class="fa fa-shopping-cart"></i>Comprar</a>
                <p class="buttons">
                    <a href="shop-detail.html" class="btn btn-default">View detail</a>
                </p>

            </div>
            <!-- /.text -->
        </div>
        <!-- /.product -->
    </div>

    <div class="col-md-3 col-sm-4">
        <div class="product">
            <div class="image">
                <a href="shop-detail.html">
                    <img src="img/product3.jpg" alt="" class="img-responsive image1">
                </a>
            </div>
            <!-- /.image -->
            <div class="text">
                <h3><a href="shop-detail.html">White Blouse Armani</a></h3>
                <p class="price"><del>$280</del> $143.00</p>
                <a href="shop-basket.html" class="btn btn-template-main"><i class="fa fa-shopping-cart"></i>Comprar</a>
                <p class="buttons">
                    <a href="shop-detail.html" class="btn btn-default">View detail</a>
                </p>
            </div>
            <!-- /.text -->

            <div class="ribbon sale">
                <div class="theribbon">SALE</div>
                <div class="ribbon-background"></div>
            </div>
            <!-- /.ribbon -->

            <div class="ribbon new">
                <div class="theribbon">NEW</div>
                <div class="ribbon-background"></div>
            </div>
            <!-- /.ribbon -->
        </div>
        <!-- /.product -->
    </div>

    <div class="col-md-3 col-sm-4">
        <div class="product">
            <div class="image">
                <a href="shop-detail.html">
                    <img src="img/product4.jpg" alt="" class="img-responsive image1">
                </a>
            </div>
            <!-- /.image -->
            <div class="text">
                <h3><a href="shop-detail.html">White Blouse Versace</a></h3>
                <p class="price">$143.00</p>
                <a href="shop-basket.html" class="btn btn-template-main"><i class="fa fa-shopping-cart"></i>Comprar</a>
                <p class="buttons">
                    <a href="shop-detail.html" class="btn btn-default">View detail</a>
                </p>

            </div>
            <!-- /.text -->

            <div class="ribbon new">
                <div class="theribbon">NEW</div>
                <div class="ribbon-background"></div>
            </div>
            <!-- /.ribbon -->
        </div>
        <!-- /.product -->
    </div>

    <div class="col-md-3 col-sm-4">
        <div class="product">
            <div class="image">
                <a href="shop-detail.html">
                    <img src="img/product2.jpg" alt="" class="img-responsive image1">
                </a>
            </div>
            <!-- /.image -->
            <div class="text">
                <h3><a href="shop-detail.html">White Blouse Versace</a></h3>
                <p class="price">$143.00</p>
                <a href="shop-basket.html" class="btn btn-template-main"><i class="fa fa-shopping-cart"></i>Comprar</a>
                <p class="buttons">
                    <a href="shop-detail.html" class="btn btn-default">View detail</a>
                </p>

            </div>
            <!-- /.text -->

            <div class="ribbon new">
                <div class="theribbon">NEW</div>
                <div class="ribbon-background"></div>
            </div>
            <!-- /.ribbon -->
        </div>
        <!-- /.product -->
    </div>

    <div class="col-md-3 col-sm-4">
        <div class="product">
            <div class="image">
                <a href="shop-detail.html">
                    <img src="img/product1.jpg" alt="" class="img-responsive image1">
                </a>
            </div>
            <!-- /.image -->
            <div class="text">
                <h3><a href="shop-detail.html">Fur coat</a></h3>
                <p class="price">$143.00</p>
                <a href="shop-basket.html" class="btn btn-template-main"><i class="fa fa-shopping-cart"></i>Comprar</a>
                <p class="buttons">
                    <a href="shop-detail.html" class="btn btn-default">View detail</a>
                </p>

            </div>
            <!-- /.text -->
        </div>
        <!-- /.product -->
    </div>
    <!-- /.col-md-4 -->
    --}}

</div> <!-- /.products -->