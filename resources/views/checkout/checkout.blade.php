@extends('template.layout')

@section('content')

    <div id="heading-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Finalizar Pedido</h1>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a>
                        </li>
                        <li>Finalizar pedido</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <div id="content">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <p class="text-muted lead">Você possui {{ $cart_summary->totalQuantity }} itens no carrinho</p>
                </div>


                <div class="col-md-9 clearfix" id="basket">

                    <div class="box">

                        <form id="payment-form" method="post" action="/pay">
                            {{ csrf_field() }}
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Product</th>
                                        <th>Quantity</th>
                                        <th>Unit price</th>
                                        <th colspan="2">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cart_items as $product)
                                    <tr>
                                        <td>
                                            <a href="#">
                                                <img src="img/detailsquare.jpg" alt="White Blouse Armani">
                                            </a>
                                        </td>
                                        <td><a href="#">{{  $product->name }}</a>
                                            <p>by <a href="seller/{{ $product->seller->slug }}">{{$product->seller->name }}</a></p>
                                        </td>
                                        <td>
                                            <input type="number" value="{{  $product->quantity }}" class="form-control" disabled="disabled">
                                        </td>
                                        <td>R$ {{ number_format($product->price, 2, ",", ".") }}</td>
                                        <td>R$ {{ number_format($product->price * $product->quantity, 2, ",", ".") }}</td>
                                        <td><a href="#"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="5">Total</th>
                                        <th colspan="2">$446.00</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                            <!-- /.table-responsive -->

                            <div class="row text-center">

                                <div class="col-md-10">
                                    <form>
                                        <div class="col-md-12">
                                            <div class="heading">
                                                <h2>Dados Pessoais</h2>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="firstname">Nome Completo</label>
                                                    <input type="text" class="form-control" id="firstname" name="customer[name]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="text" class="form-control" id="email" name="customer[email]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cpf">CPF</label>
                                                    <input type="text" class="form-control" id="cpf" name="customer[document]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="birthday">Data de Nascimento</label>
                                                    <input type="text" class="form-control" id="birthday" name="customer[birthday]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="phone">Telefone</label>
                                                    <input type="text" class="form-control" id="phone" name="customer[phone]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="gender">Sexo</label>
                                                    <select class="form-control" id="gender" name="customer[gender]">
                                                        <option value="M">Masculino</option>
                                                        <option value="F">Feminino</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->


                                        <div class="col-md-12">
                                            <div class="heading">
                                                <h2>Endereço</h2>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="street">Rua</label>
                                                    <input type="text" class="form-control" id="street" name="address[street]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="streetNumber">Número</label>
                                                    <input type="text" class="form-control" id="streetNumber" name="address[streetNumber]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="neighborhood">Bairro</label>
                                                    <input type="text" class="form-control" id="neighborhood" name="address[neighborhood]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="zipcode">CEP</label>
                                                    <input type="text" class="form-control" id="zipcode" name="address[zipcode]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="city">Cidade</label>
                                                    <input type="text" class="form-control" id="city" name="address[city]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="state">Estado</label>
                                                    <input type="text" class="form-control" id="state" name="address[state]" maxlength="2">
                                                </div>
                                            </div>
                                        </div><!-- /.row -->


                                        <div class="col-md-12">
                                            <div class="heading">
                                                <h2>Pagamento</h2>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group col-sm-6 row">
                                                    <label for="installments">Parcelas</label>
                                                    <select class="form-control" id="installments" name="creditCard[installments]">
                                                        @for ($i = 1; $i <= 12; $i++)
                                                            <option value="{{ $i }}">Em {{ $i }}x de R$ {{ number_format(($cart_summary->totalAmount + 42) / $i, 2, ",", ".") }} s/ juros</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cardNumber">Número do cartão</label>
                                                    <input type="text" class="form-control" id="cardNumber" name="creditCard[cardNumber]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="holderName">Nome escrito no cartão</label>
                                                    <input type="text" class="form-control" id="holderName" name="creditCard[holderName]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Data de validade</label>
                                                    <input type="text" class="form-control" id="month" name="creditCard[month]" maxlength="2" placeholder="Mês">
                                                    <input type="text" class="form-control" id="year" name="creditCard[year]" maxlength="4" placeholder="Ano">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cvv">CVV</label>
                                                    <input type="text" class="form-control" id="cvv" name="creditCard[cvv]" maxlength="4">
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </form>

                                </div>
                            </div>

                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="/" class="btn btn-default"><i class="fa fa-chevron-left"></i> Continuar Comprando</a>
                                </div>
                                <div class="pull-right">
                                    <button id="do-pay" type="button" class="btn btn-template-main">Processar Pedido <i class="fa fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- /.box -->

                    {{--
                    <div class="row">
                        <div class="col-md-3">
                            <div class="box text-uppercase">
                                <h3>You may also like these products</h3>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="product">
                                <div class="image">
                                    <a href="shop-detail.html">
                                        <img src="img/product2.jpg" alt="" class="img-responsive image1">
                                    </a>
                                </div>
                                <div class="text">
                                    <h3><a href="shop-detail.html">Fur coat</a></h3>
                                    <p class="price">$143</p>

                                </div>
                            </div>
                            <!-- /.product -->
                        </div>

                        <div class="col-md-3">
                            <div class="product">
                                <div class="image">
                                    <a href="shop-detail.html">
                                        <img src="img/product3.jpg" alt="" class="img-responsive image1">
                                    </a>
                                </div>
                                <div class="text">
                                    <h3><a href="shop-detail.html">Fur coat</a></h3>
                                    <p class="price">$143</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>

                        <div class="col-md-3">
                            <div class="product">
                                <div class="image">
                                    <a href="shop-detail.html">
                                        <img src="img/product1.jpg" alt="" class="img-responsive image1">
                                    </a>
                                </div>
                                <div class="text">
                                    <h3><a href="shop-detail.html">Fur coat</a></h3>
                                    <p class="price">$143</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>

                    </div>
                    --}}
                </div>
                <!-- /.col-md-9 -->

                <div class="col-md-3">
                    <div class="box" id="order-summary">
                        <div class="box-header">
                            <h3>Resumo da Compra</h3>
                        </div>
                        <p class="text-muted">O valor do frete é calculado automaticamente.</p>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Subtotal do pedido</td>
                                    <th>R$ {{  number_format($cart_summary->totalAmount, 2, ",", ".") }}</th>
                                </tr>
                                <tr>
                                    <td>Valor do frete</td>
                                    <th>R$ 42,00</th>
                                </tr>
                                <tr class="total">
                                    <td>Total</td>
                                    <th>R$ {{  number_format($cart_summary->totalAmount + 42, 2, ",", ".") }}</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>


                    {{--
                    <div class="box">
                        <div class="box-header">
                            <h4>Coupon code</h4>
                        </div>
                        <p class="text-muted">If you have a coupon code, please enter it in the box below.</p>
                        <form>
                            <div class="input-group">

                                <input type="text" class="form-control">

                                <span class="input-group-btn">

                                    <button class="btn btn-template-main" type="button"><i class="fa fa-gift"></i></button>

                                </span>
                            </div>
                            <!-- /input-group -->
                        </form>
                    </div>
                    --}}

                </div>
                <!-- /.col-md-3 -->
            </div>

        </div>
        <!-- /.container -->
    </div>
@stop