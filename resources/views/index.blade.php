@extends('template.layout')

@section('content')
    <section>
        <!-- *** HOMEPAGE CAROUSEL ***
        _________________________________________________________ -->

        <div class="home-carousel">

            <div class="dark-mask"></div>

            <div class="container">
                <div class="homepage owl-carousel">
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-5 right">
                                <h1>Fantasias de diversos fornecedores</h1>
                                <p>Melhores escolhas para seu carnaval</p>
                            </div>
                            <div class="col-sm-7">
                                <img class="img-responsive" src="img/template-homepage.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">

                            <div class="col-sm-7 text-center">
                                <img class="img-responsive" src="img/template-mac.png" alt="">
                            </div>

                            <div class="col-sm-5">
                                <h2>Diferentes modelos para sua festa</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    <br/> Phasellus nec condimentum nulla, quis luctus ex. Vestibulum cursus ligula turpis, in pellentesque lacus efficitur ac. Cras tristique id arcu eu laoreet.</p>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.project owl-slider -->
            </div>
        </div>

        <!-- *** HOMEPAGE CAROUSEL END *** -->
    </section>

    <section class="bar background-white">
        <div class="container">
            <div class="col-md-12">

                @include('products.showcase')

            </div>
        </div>
    </section>

    <section class="bar background-image-fixed-2 no-mb color-white text-center">
        <div class="dark-mask"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="icon icon-lg"><i class="fa fa-file-code-o"></i>
                    </div>
                    <h3 class="text-uppercase">Alugue sua fantasia com apenas um clique!</h3>
                    <p class="lead">São diversos modelos pra você escolher e se divertir no carnaval.</p>
                    <p class="text-center">
                        <a href="index2.html" class="btn btn-template-transparent-black btn-lg">Fique por dentro das novidades</a>
                    </p>
                </div>

            </div>
        </div>
    </section>

    <section class="bar background-gray no-mb">
        <div class="container">
            <div class="row">
                {{--
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h2>Our clients</h2>
                    </div>

                    <ul class="owl-carousel customers">
                        <li class="item">
                            <img src="img/customer-1.png" alt="" class="img-responsive">
                        </li>
                        <li class="item">
                            <img src="img/customer-2.png" alt="" class="img-responsive">
                        </li>
                        <li class="item">
                            <img src="img/customer-3.png" alt="" class="img-responsive">
                        </li>
                        <li class="item">
                            <img src="img/customer-4.png" alt="" class="img-responsive">
                        </li>
                        <li class="item">
                            <img src="img/customer-5.png" alt="" class="img-responsive">
                        </li>
                        <li class="item">
                            <img src="img/customer-6.png" alt="" class="img-responsive">
                        </li>
                    </ul>
                    <!-- /.owl-carousel -->
                </div>
                --}}
            </div>
        </div>
    </section>
@stop