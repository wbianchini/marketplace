$(window).load(function () {
    $('.add-to-cart').on('click', function(){
        let product_id = $(this).attr('product_id');
        let token = $('[name="_token"]').val();

        $.ajax({
            url: '/store',
            type: 'POST',
            data: {
                _token: token,
                product_id: product_id
            },
            success: function (response) {
                console.log(response);
                window.location.href = '/checkout';
            }
        });
    });

    $('#do-pay').on('click', function(){
        // todo validate fields

        $('#payment-form').submit();
    });

    /**
     * Masks
     */
    $('#cvv').mask('0000');
    $('#month').mask('00');
    $('#year').mask('0000');
    $('#zipcode').mask('00000-000');
    $('#cpf').mask('000.000.000-00');
    $('#birthday').mask('00/00/0000');
    $('#cardNumber').mask('0000 0000 0000 0000');
});