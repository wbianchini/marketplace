<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'name'  => $faker->name,
        'price' => $faker->randomFloat(2, 0, 200    ),
        'image_source' => $faker->imageUrl(),
        'slug' => $faker->unique()->slug(2),
        'seller_id' => function() {
            return \App\Models\Seller::inRandomOrder()->first()
                ?? factory(\App\Models\Seller::class)->create()->id;
        }
    ];
});