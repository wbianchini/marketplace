<?php

use Faker\Generator as Faker;

$factory->define(App\Models\SellerCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'percentage' => $faker->randomFloat(2, 0.5, 0.8)
    ];
});