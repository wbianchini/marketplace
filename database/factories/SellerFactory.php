<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Seller::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'category_id' => function() {
        	return App\Models\SellerCategory::where('name', '<>', 'Owner')->inRandomOrder()->first()
                ?? factory(App\Models\SellerCategory::class)->create()->id;
        },
        'slug' => $faker->unique()->slug(2),
    ];
});
