<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('category_id');
            $table->string('slug')->unique();
            $table->string('bank_account')->nullable();
            $table->integer('transfer_day')->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();

            $table->foreign('category_id')
                  ->references('id')->on('seller_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
