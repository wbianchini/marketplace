<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->double('amount', 8, 2);
            $table->integer('status_id');
            $table->integer('customer_id');
            $table->boolean('active')->default(1);
            $table->timestamps();

            $table->foreign('customer_id')
                  ->references('id')->on('customers');
            $table->foreign('status_id')
                  ->references('id')->on('order_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
