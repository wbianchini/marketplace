<?php

use App\Models\Seller;
use App\Models\SellerCategory;
use Illuminate\Database\Seeder;

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_id = SellerCategory::where('name', 'Owner')
                ->where('active', true)
                ->first()->id;

        Seller::insert([
            'name' => 'Maria Barros',
            'category_id' => $category_id,
            'slug' => 'maria-barros',
            'active' => true,
        ]);

        factory(\App\Models\Seller::class, 2)->create();
    }
}
