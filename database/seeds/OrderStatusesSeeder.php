<?php

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =[
            ['id' => 1, 'status' => 'processing'],
            ['id' => 2, 'status' => 'authorized'],
            ['id' => 3, 'status' => 'paid'],
            ['id' => 4, 'status' => 'refunded'],
            ['id' => 5, 'status' => 'waiting_payment'],
            ['id' => 6, 'status' => 'pending_refund'],
            ['id' => 7, 'status' => 'refused'],
            ['id' => 8, 'status' => 'chargedback'],
        ];
        OrderStatus::insert($data);
    }
}
