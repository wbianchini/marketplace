<?php

use App\Models\SellerCategory;
use Illuminate\Database\Seeder;

class SellerCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SellerCategory::insert([
            'name' => 'Owner',
            'percentage' => 1,
            'active' => true,
        ]);

        factory(\App\Models\SellerCategory::class, 2)->create();
    }
}
