<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(SellerCategorySeeder::class);
         $this->call(SellerSeeder::class);
         $this->call(ProductSeeder::class);
         $this->call(OrderStatusesSeeder::class);
    }
}
